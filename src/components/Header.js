import React from 'react';
import logo from '../resources/Toluna-Logo.png'

const headerStyle = {
    height: 70,
    width: 194
};
class Header extends React.Component {
    render() {
        return (
            <header>
                <img src={logo} style={headerStyle} alt="Logo" />
            </header>
        );
    }
}


export default Header;
