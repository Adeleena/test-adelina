import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import desktopBG from "./resources/BG_Desktop.png";
import mobilepBG from "./resources/BG_Mobile.png";

let initialData = JSON.parse(JSON.stringify(require("./resources/data.json")))


class App extends React.Component {
  constructor() {

    super();
    this.state = {
      deviceType: window.innerWidth >= 650 ? desktopBG : mobilepBG,
      styles: window.innerWidth >= 650 ? desktopStyles : mobileStyles,
      buttonTxt: "EDIT",
      questionValue: "",
      answers: [{ text: "", imageURL: "" }],

    }
  }

  updateDimensions() {
    if (window.innerWidth <= 650) {
      this.setState({
        deviceType: mobilepBG,

        styles: mobileStyles
      });
    } else {
      this.setState({
        deviceType: desktopBG,
        styles: desktopStyles
      })

    }
  }
  changeButtonText() {
    this.setState({ buttonTxt: "SAVE" })

  }
  handleChange(event) {
    this.setState({ questionValue: event.target.value })
  }

  saveToLocalStorage() {
    console.log(this.state.questionValue)
    localStorage.setItem(this.state.questionValue, "1")
    //to be continued with the answers
  }


  render() {
    let { answers } = this.state.answers
    return (
      <div className="App" style={{ backgroundImage: 'url(require({state.deviceType}))' }} >

        <Header />
        <div id="main-area" >
          <img src={initialData.question.imageURL} alt={initialData.question.text} style={this.state.styles.mainImg} />
          <br />
          <div class="question-container" style={this.state.styles.mainSpacing}>
            <label ><span>Question</span></label><br />
            <div class="wrapper">
              <input name="questionl" id="question" placeholder={initialData.question.text} style={this.state.styles.inputSettings} onChange={this.changeButtonText.bind(this)} onBlur={this.handleChange.bind(this)} vaulue="" />
              <button class="button" id="edit" onClick={this.saveToLocalStorage.bind(this)}>{this.state.buttonTxt}  </button>
            </div>
          </div>
          <div class="answes-input-container">
            <label >Answers</label><br />
            <input name="answes" id="answes" placeholder="Search Answers" style={this.state.styles.inputSettings} />
            <button class="button" id="search" >SEARCH</button>
          </div>
        </div>


        <h3>Image<t /> Text </h3>
        <div id="mid-area" >
          {initialData.answers.map(ans =>
            <table >
              <tbody>
                <tr >
                  <td><img src={ans.imageURL} alt={ans.text} style={this.state.styles.midImg} /> </td>
                  <td> {ans.text}</td>
                  <td width="90%" align="right"><button class="button" id="stickyButton" ><img src="https://image.flaticon.com/icons/png/512/78/78076.png" alt='remove' style={desktopStyles.removeBttn} /></button> </td>
                </tr>
              </tbody>
            </table>
          )}
        </div>




        {/*         
        <div class="question-container">
          <label for="question">Question</label><br />
          <input name="questionl" id="question" placeholder={initialData.question.text} />
          <button className="input-button">edit</button>
        </div>  */}
      //to also be continued

      </div>
      // </div>
    );
  }
}



const desktopStyles = {
  mainImg: {
    height: 105,
    width: 105
  },
  midImg: {
    height: 93,
    width: 93
  },
  mainSpacing: {
    margin: 61
  },
  inputSettings: {
    width: 600
  },
  possibleAnswer: {
    fontSize: 22
  },
  removeBttn: {
    height: 64,
    width: 64
  }
};
const mobileStyles = {
  mainImg: {
    height: 105,
    width: 105
  },
  midImg: {
    height: 119,
    width: 119
  },
  mainSpacing: {
    margin: 56
  },
  inputSettings: {
    width: 600
  },
  possibleAnswer: {
    fontSize: 55
  },
  removeBttn: {
    height: 160,
    width: 160
  }
};
export default App;
